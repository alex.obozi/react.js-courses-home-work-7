import {
  GET_POSTS_REQ,
  GET_POSTS_RES
} from "../actions";

const initialState = {
  loading: false,
  loaded: false,
  data: [],
  error: false
};

const posts = (state = initialState, action) => {
  switch (action.type){
    case GET_POSTS_REQ:
      return{
        ...state,
        loading: true
      };
    case GET_POSTS_RES:
      return{
        ...state,
        loaded: true,
        data: action.payload
      };
    default:
      return state;
  }
};

export default posts;
