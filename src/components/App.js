import React from 'react';
import './App.css';
import List from "./List";
import { Link, Route } from 'react-router-dom';
import Post from "./Post";
import {connect} from "react-redux";
import {getPosts} from "../actions";

class App extends React.Component{

  componentDidMount() {
    this.props.loadPosts();
  };

  render(){

    return (
      <div className="App">
        <nav>
          <Link to='/'>Home</Link>
        </nav>
        <Route exact path='/' component={List}/>
        <Route exact path='/post/:postid' component={Post} />
        <Route exact path='/user/:userid' component={List} />
      </div>
    );

  }

}

const mapDispatchToProps = (dispatch) => ({
  loadPosts: () => {
    dispatch(getPosts());
  }
});

export default connect(null, mapDispatchToProps)(App);
