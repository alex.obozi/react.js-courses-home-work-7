import React, {Component} from 'react';

import {Link} from 'react-router-dom';

import { connect } from 'react-redux';

export class List extends Component {

  state = {

    countOfLoadPosts: 50,
    showButton: true
  };

  loadMorePosts = (countOfCurrentPosts) => {

    let count = countOfCurrentPosts + 25;
    let maxLengthPosts = this.props.posts.length;

    if(count <= maxLengthPosts){
      this.setState({
        countOfLoadPosts: count
      });

      if( count === maxLengthPosts ){
        this.setState({
          showButton: false
        });
      }
    }

  };

  showPosts = (checkParam) => {
    const {loadMorePosts} = this;
    const {posts} = this.props;
    const {countOfLoadPosts, showButton} = this.state;
    const filteredByUserPosts = posts.filter(item => item.userId.toString() === checkParam);

    if(checkParam !== undefined){
      return(
        <ul>
          {
            filteredByUserPosts.map(item =>
              <li key={item.id}>
              <Link to={`/post/${item.id}`}>
                User Id: {item.userId} Title of post: {item.title}
              </Link>
            </li>
            )
          }
        </ul>
      )
    }else{
      return(
        <>
          <ul>
            {
              posts.slice(0, countOfLoadPosts).map(post => <li key={post.id}>
                  <Link to={`/post/${post.id}`}>
                    {post.title}
                  </Link>
                </li>
              )
            }
          </ul>
          {
            showButton && <button onClick={() => loadMorePosts(countOfLoadPosts)}>Load more</button>
          }

        </>
      )
    }
  };

  render = () => {

    const {showPosts} = this;
    const {loaded} = this.props;

    return (
      <div>
        <h2>List</h2>
        {
          loaded ?
            showPosts(this.props.match.params.userid)
            :
            <p>Loading...</p>
        }

      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return({
    posts: state.posts.data,
    loaded: state.posts.loaded
  });
};

export default connect(mapStateToProps)(List);
