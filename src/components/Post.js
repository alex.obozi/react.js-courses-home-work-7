import React, {Component} from 'react';

import { connect } from 'react-redux';

import { Link } from "react-router-dom";

class Post extends Component {

  render() {

    const {posts, loaded} = this.props;
    const postId = this.props.match.params.postid - 1;

    return (
      <div>
        {
          loaded ?
            <div>
              <ul>
                {
                  Object.values(posts[postId]).map((item, id) => <li key={id}>{item}</li>)
                }
              </ul>
              <Link to={`/user/${posts[postId].userId}`}>
                Author posts
              </Link>
            </div>
            :
            <p>Loading...</p>
        }
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return({
    posts: state.posts.data,
    loaded: state.posts.loaded
  });
};


export default connect(mapStateToProps)(Post);
