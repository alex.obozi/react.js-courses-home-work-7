
export const GET_POSTS_REQ = 'GET_POSTS_REQ';
export const GET_POSTS_RES = 'GET_POSTS_RES';


export const getPosts = () => (dispatch) => {
  fetch('https://jsonplaceholder.typicode.com/posts/')
    .then( res => res.json() )
    .then( res => {
      dispatch({
        type: GET_POSTS_RES,
        payload: res
      });
    });
};
